T = 5e5;
N = 1000;
Rs = linspace(-T/2, T/2, 2*N-1);
Vs = linspace(0,    T,   N);
[R, V] = meshgrid(Rs, Vs);
E = lnevidence(R, V);

imagesc(Rs, Vs, E);
axis xy;
colorbar
