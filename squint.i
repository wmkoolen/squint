/* Squint
   Wouter M. Koolen <wmkoolen@gmail.com>

   Binding for Python. Tested with python version 2.7
 */

%module squint
%{

  #include "squint.h"

  // wrap the C++ class method to a function
  double lnevidence(double R, double V) {
    return Squint<double>::lnevidence(R, V);
  }
%}

// this function is the entry point
double lnevidence(double R, double V);
