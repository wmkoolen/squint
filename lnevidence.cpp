#include "mex.h"
#include "squint.h"
using namespace std;


/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    if(nrhs != 2) {
        mexErrMsgIdAndTxt("squint:nrhs","Two inputs required (R and V).");
    }

    if(nlhs > 1) {
        mexErrMsgIdAndTxt("squint:nlhs","One output required.");
    }

    if(!mxIsDouble(prhs[0])) {
        mexErrMsgIdAndTxt("squint:notDouble","First input (R) must be of type double.");
    }

    if(!mxIsDouble(prhs[1])) {
        mexErrMsgIdAndTxt("squint:notDouble","Second input (V) must be of type double.");
    }

    const size_t   ndimR = mxGetNumberOfDimensions(prhs[0]);
    const mwSize * dimsR = mxGetDimensions(prhs[0]);    
    const size_t   ndimV = mxGetNumberOfDimensions(prhs[1]);
    const mwSize * dimsV = mxGetDimensions(prhs[1]);

    if (ndimR != ndimV) {
      mexErrMsgIdAndTxt("squint:incompatible","Inputs must have the same number of dimensions.");
    }
    
    for (size_t i = 0; i < ndimR; ++i) {
      if (dimsR[i] != dimsV[i]) {
	mexErrMsgIdAndTxt("squint:incompatible","Inputs must have the same sizes.");
      }
    }

    const size_t nelts = mxGetNumberOfElements(prhs[0]);
    assert(nelts == mxGetNumberOfElements(prhs[1]));

    /* get R and V */
    const double * R = mxGetPr(prhs[0]);
    const double * V = mxGetPr(prhs[1]);

    /* create the output matrix */
    plhs[0] = mxCreateNumericArray(ndimR, dimsR, mxDOUBLE_CLASS, mxREAL);

    /* get a pointer to the real data in the output matrix */
    double * lE = mxGetPr(plhs[0]);

    for (size_t i = 0; i < nelts; ++i) {
      if (V[i] < 0) mexErrMsgIdAndTxt("squint:negative","Negative V.");
      lE[i] = Squint<long double>::lnevidence(R[i], V[i]);
    }
}
