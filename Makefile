.DELETE_ON_ERROR:
.PHONY: compile all matlab octave python clean test


CXXFLAGS := -std=gnu++11 -g -O3 -Wall -pipe -march=native -MMD

EXECS := test_squint dump
MEXS := lnevidence.mex
MEXAS := lnevidence.mexa64

DEPS := $(EXECS:=.d) $(MEXS:.mex=.d) $(MEXAS:.mexa64=2.d) _squint.d

# run 'make PYCONF=python-config' for python 2
PYCONF := python3-config


compile : $(EXECS)   # default target
octave : $(MEXS)
matlab : $(MEXAS)
python : _squint.so
all : compile matlab octave

# automatically track "include" dependencies (they are output by gcc -MMD)
-include $(DEPS)


# Default rule puts all dependencies as arguments. We put only the main cpp file
$(EXECS) : % : %.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

# Octave
$(MEXS) : %.mex : %.cpp
	CXX="$(CXX)" CXXFLAGS="$(CXXFLAGS) -MQ '$@'" mkoctfile --mex $<

# Matlab
$(MEXAS) : %.mexa64 : %.cpp
	$(shell matlab -e | grep MATLAB= | cut -d = -f 2-)/bin/mex -cxx CXX="$(CXX)" CXXFLAGS="$(CXXFLAGS) -fPIC -MF $*2.d" $<

# Python
squint_wrap.cpp : squint.i
	swig -c++ -python -o $@ $<

_squint.so : squint_wrap.cpp
	$(CXX) $(CXXFLAGS) $(patsubst -I%, -isystem%, $(filter-out -Wstrict-prototypes, $(shell $(PYCONF) --cflags --ldflags))) -fpic -shared $< -o $@


dump.dat : dump
	./$< > $@

clean:
	rm -rf $(EXECS) $(MEXS) $(MEXAS) $(DEPS) _squint.so squint_wrap.cpp squint.py squint.pyc


test : test_squint
	./test_squint
