# exercise all make targets (octave, matlab, python, python3)
# with both the gcc and clang compilers

set -e

for c in g++ clang++; do
    make clean
    make CXX=$c test

    make clean
    make CXX=$c octave matlab
    octave --eval 'lnevidence(3,2)'

    make clean
    for p in python python3; do
	make CXX=$c clean
	make CXX=$c PYCONF=${p}-config python
	$p -c 'import squint; print(squint.lnevidence(3,2));';
    done
done

make clean
