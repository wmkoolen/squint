# Squint README #

This repository contains a numerically stable implementation of the Squint algorithm from the paper

*
[Second-Order Quantile Methods for Experts and Combinatorial Games](http://jmlr.csail.mit.edu/proceedings/papers/v40/Koolen15a.pdf)  
Koolen, Wouter M., and Tim van Erven  
In Proceedings of the 28th Annual Conference on Learning Theory (COLT) 2015, 1155–75
*

It also contains example scripts that run Squint on IID random data.
See [run.m](run.m) for a version in Matlab/Octave, and [run_squint.py](run_squint.py) for a version in Python.



## Requirements ##
* The implementation is in C++. It comes with a Makefile for compilation using GCC or Clang.
* The [run.m](run.m) example script works in both Matlab and Octave.
* The [run_squint.py](run_squint.py) example script works in Python 3. It uses numpy and matplotlib. Compilation additionally requires swig and python3-dev.

## Set up ##
1. Get the source code by cloning this git repository
2. For Matlab/Octave
    1. Run 'make octave' or 'make matlab'
    2. Fire up octave or matlab
    3. Execute 'run'. This will run Squint on stochastic IID data and graph the results.
3. For Python
    1. Run 'make python'
    2. Execute 'python3 run_squint.py'. This will run Squint on stochastic IID data and graph the results.

If you want to see the core code, start with [squint.h](squint.h). If you want to try squint on your data, adapt [run.m](run.m) or [run_squint.py](run_squint.py).

## How to run tests ##
The repository contains a battery of unit tests that check the implementation against more direct expressions (which are unstable in some regions of the parameter space).

* Run 'make test' to compile and execute the test executable.
* The reported precision should be around 15 digits for *long double* arithmetic.

## More information ##

* [Squint paper](http://jmlr.csail.mit.edu/proceedings/papers/v40/Koolen15a.pdf)
* [Blog post: Discussion of implementation specifics](http://blog.wouterkoolen.info/Squint_implementation/post.html)
* [Blog post: Performance guarantees supplement](http://blog.wouterkoolen.info/Squint_PAC/post.html)