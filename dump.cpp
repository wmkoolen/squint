/* Dump the squint evidence
   Wouter M. Koolen <wmkoolen@gmail.com>

   Evaluate the Squint evidence function on a grid and output in
   format suitable for GNU Octave

   usage:

   1) compile this executable
   > make dump

   2) create the dat file
   > ./dump > dump.dat

   3) start octave
   > octave (or octave --no-gui)

   4) load the data
   >> load("dump.dat")

   5) make the graph
   >> imagesc(Rs, Vs, E)
   >> axis xy
   >> colorbar
   or
   >> surf(Rs, Vs, E)
   >> shading flat
   >> colorbar

 */

#include <iostream>
#include <cfenv>
#include "squint.h"
#include "LinSpace.h"
using namespace std;


// Output range to GNU Octave 
void print_range(const char * name, const LinSpace & R) {
  cout << "# name: " << name << endl
       << "# type: matrix" << endl
       << "# rows: " << R.steps << endl
       << "# columns: " << 1 << endl;
  for (int i = 0; i < R.steps; ++i) {
    double v = R.get(i);
    cout << v << endl;
  }
  cout << endl;

}

// Output Squint log-evidence to GNU Octave 
void print_table(const char * name, const LinSpace & Rs, const LinSpace & Vs) {

  cout << "# name: " << name << endl
       << "# type: matrix" << endl
       << "# rows: " << Vs.steps << endl
       << "# columns: " << Rs.steps << endl;

  for (int vi = 0; vi < Vs.steps; ++vi) {
    double V = Vs.get(vi);
    for (int ri = 0; ri < Rs.steps; ++ri) {
      double R = Rs.get(ri);
      cout << Squint<long double>::lnevidence(R, V) << " ";
    }
    cout << endl;
  }
  cout << endl;
}



int main(int argc, const char ** argv) {

  int T = argc < 2 ? 5e5  : atoi(argv[1]);
  int N = argc < 3 ? 1000 : atoi(argv[2]);

  LinSpace Rs(-T, T, 2*N-1);
  LinSpace Vs(0, T, N);

  print_range("Rs", Rs);
  print_range("Vs", Vs);

  print_table("E", Rs, Vs);

  return 0;
}
