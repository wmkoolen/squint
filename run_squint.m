%% run the Squint algorithm on losses
%  Wouter M. Koolen <wmkoolen@gmail.com>
%
% input:
%   pi:     1xK vector of positive prior weights
%   losses: TxK matrix of per-round expert losses
%
% output:
%   lalg:   Tx1 vector of losses of the Squint algorithm
%   R:      TxK matrix of cumulative regrets
%   V:      TxK matrix of cumulative excess loss squared (variance).

function [lalg, R, V] = run_squint(pi, losses)

[T, K] = size(losses);

assert(max(losses(:)) - min(losses(:)) <= 1);

% storage for cumulative regrets and variances
R = nan(1+T, K);
V = nan(1+T, K);

% storage for instantaneous loss of Squint
lalg = nan(T,1);

% set cumulative regrets and variances after T=0 rounds to zero.
% (note: I use the 1+. prefix to simulate zero-based indexing)
% so R(1+0,:) is the situation after T=0 rounds.
R(1+0,:) = zeros(1, K);
V(1+0,:) = zeros(1, K);

lnpi = log(pi); % log of prior

%% Run Squint
for t=1:T
  % Numerically stable way to compute the weights
  lw = lnpi + lnevidence(R(1+t-1,:), V(1+t-1,:));
  w = exp(lw - max(lw));
  w = w / sum(w);

  lalg(t) = w * losses(t,:)';   % instantaneous loss of Squint
  r = lalg(t) - losses(t,:);    % vector of instantaneous regrets
  R(1+t,:) = R(1+t-1,:) + r;    % update cumulative regret
  V(1+t,:) = V(1+t-1,:) + r.^2; % update cumulative variance

  if round(100*t/T) ~= round(100*(t-1)/T)
    fprintf(2, '%d%%\r', round(100*t/T)); % progress report
  end
end

